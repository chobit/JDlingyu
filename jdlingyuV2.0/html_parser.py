#!/usr/bin/Python
# -*- coding: utf-8 -*-
import re
from bs4 import BeautifulSoup


class HtmlParder(object):
    def parser_imgs_link(self, page_count):
        link = set()
        soup = BeautifulSoup(page_count, 'html.parser', from_encoding='utf-8')
        #http: // www.jdlingyu.moe / 22095 /
        img_urls = soup.find_all('a',href=re.compile(r'http://www.jdlingyu.moe/\d{5}/'))
        for img_link in img_urls:
            link.add(img_link['href'])
        return link
    def parser_img(self, img_cont):
        links =set()
        soup = BeautifulSoup(img_cont, 'lxml')
        #<a href="http://www.jdlingyu.moe/wp-content/uploads/2016/02/2017-02-15_23-55-48.jpg" class="phzoom"><img title="" alt="" src="http://www.jdlingyu.moe/wp-content/uploads/2016/02/2017-02-15_23-55-48.jpg" width="853" height="1280"><span class="ph_hover" style="display: none;"></span></a>
        img_lsit = soup.select('div.main-body > p > a')
        img_title = soup.select('h2.main-title')[0].text
        for img in img_lsit:
            links.add(img.get('href'))
        data = {
            'title':img_title,
            'imgs':links
        }
        return data
