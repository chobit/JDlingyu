#!/usr/bin/Python
# -*- coding: utf-8 -*-
class UrlManager(object):
    def get_page_urls(self, root_url, max_page):
        # page_url = 'http://www.jdlingyu.moe/page/2/'
        page_urls = set()
        for page in range(1,int(max_page)+1):
            page_url = 'http://www.jdlingyu.moe/page/%s/' % page
            page_urls.add(page_url)
        return page_urls