#!/usr/bin/Python
# -*- coding: utf-8 -*-
import urllib2


class HtmlDownloader(object):
    def downloaderHtml(self, url):
        if url is None:
            return
        # request = urllib2.Request(url, headers=self.headers)
        response = urllib2.urlopen(url)
        if response.getcode() != 200:
            return None
        return response.read()